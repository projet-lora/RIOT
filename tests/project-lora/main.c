/*
 *  THE SHOT-ALARM
 * @author      Mathis ESCALLIER <mathisescallier@gmail.com>
 *              Clément SARTEL <clementsartel2104@gmail.com>
 * @}
 */

#include <stdio.h>

/**************** INCLUDES ****************/
/*----SENSOR PIRE----*/
#include "thread.h"
#include "xtimer.h"
#include "pir.h"
#include "pir_params.h"

/*----SENSOR SCD_30----*/
#include "periph/gpio.h"
#include "xtimer.h"
#include "scd30.h"
#include "scd30/include/scd30_params.h"
#include "scd30/include/scd30_internal.h"

/* TODO: Add the cayenne_lpp header here */
#include "cayenne_lpp.h"
#include "net/loramac.h"
#include "semtech_loramac.h"

/*LCD*/
#include "LCD/hd44780.h"
#include "LCD/hd44780.c"
#include "LCD/hd44780_params.h"
#include "LCD/hd44780_internal.h"

/*SERVO*/
#include "servo.h"
#include "periph/pwm.h"
#include "debug.h"

/*TEMPERATURE*/
#include "lm75.h"



/**************** DEFINES ****************/
#define MEASUREMENT_INTERVAL_SECS (2)


/**************** GLOBAL VARIABLES ****************/
int alarm_state = 0;       //Alarm system state (0 : off - 1 : on)
int intrusion = 0;        //intrusion detection
int button_state = 1;
int gun_state; //Gun reloading (0 : no ammo / 1 : ready to shoot)
int fire_detection = 0; //fire detection
int pollution_detection = 0; //pollution detection
int flood_detection = 0; //flood detection
//PIR sensor
static pir_t dev;
int pir_detection_increment = 0;
int pir_detection_threshold = 1;
//SCD30 sensor
scd30_t scd30_dev;
scd30_params_t params = SCD30_PARAMS;
scd30_measurement_t result;
float CO2_value = 0;
float CO2_previous_value = 0;
float TEMP_value = 0;
float TEMP_previous_value = 0;
float HUMIDITY_value = 0;
float HUMIDITY_previous_value = 0;
//GPIOS
static gpio_t BUZZER_PIN = GPIO_PIN(0, 9);  // D9   (GPIOA PIN9)
static gpio_t LED_PIN = GPIO_PIN(1, 10);    // D10  (GPIOB PIN10) 
static gpio_t PWM_PIN = GPIO_PIN(1, 5);     // D5   (GPIOB PIN5) 
static gpio_t BUTTON_PIN = GPIO_PIN(1, 4);  // A4   (GPIOB PIN4)
//static gpio_t LEDG_PIN = GPIO_PIN(1, 3);  // A3   (GPIOB PIN3) 
//static gpio_t PIR_PIN = GPIO_PIN(0, 0);   // D0   (GPIOA PIN0) 
//LORAMAC 
int sender_ite = 0;
extern semtech_loramac_t loramac; //Declare globally the loramac descriptor
static cayenne_lpp_t lpp;         //Declare globally Cayenne LPP descriptor here
static const uint8_t deveui[LORAMAC_DEVEUI_LEN] = { 0xE5, 0x3A, 0xB2, 0xA1, 0x3A, 0xDD, 0x52, 0x95 }; //Device and application informations required for OTAA activation (deveui)
static const uint8_t appeui[LORAMAC_APPEUI_LEN] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }; //Device and application informations required for OTAA activation (appeui)
static const uint8_t appkey[LORAMAC_APPKEY_LEN] = { 0x83, 0x77, 0x22, 0xCE, 0xF2, 0x21, 0xCB, 0x92, 0x07, 0x5D, 0xD8, 0x7D, 0x88, 0xCF, 0x75, 0xFE}; //Device and application informations required for OTAA activation (appkey)
//TEMPERATURE
//lm75_t lm75;

/**************** FUNCTIONS PROTOTYPES ****************/
//CAYENNE (Lora communication)
void cayenne(void);
static void sender(void);
//SCD30 SENSOR
void sensor_scd30_init(void);
int sensor_scd30_measure(void);
//PIR SENSOR
static void sensor_pir_init(void);
int sensor_pir_measurement(void);
void* pir_handler(void *arg);
//static char pir_handler_stack[THREAD_STACKSIZE_MAIN];
//ALARM
void alaarm(long int duration);

//SHOT
void pwm_shot(void);


//POWER UP
int power_up(void); 
void power_up_song(void);

//POWER DOWN
void power_down_song(void);



int main(void)
{

    printf("\nPROGRAMME MAIN() - EXECUTION\n");

    printf("\n ********** BEGIN INITIALISATION ********** \n");
    //******************************* INITIALISATION ********************************//

    //BUZZER - LED (launch alarm)
    gpio_init (BUZZER_PIN, GPIO_OUT);
    gpio_init (LED_PIN, GPIO_OUT);
    gpio_write(LED_PIN, 0); //Switch off led
    //gpio_init (LEDG_PIN, GPIO_OUT);
    //gpio_write(LEDG_PIN, 1); //Switch on led
    

    //BUTTON (on/off alarme)
    gpio_init (BUTTON_PIN, GPIO_IN_PD);
    //button_state = gpio_read(BUTTON_PIN);
    //printf("\nbutton : %d\n", button_state);

    //SCD30 SENSOR (detection of intrusion - Temperature/humidity/CO2)
    sensor_scd30_init();    //test scd30 init  

    //PIR SENSOR (detection of intrusion - movement/light detection)
    sensor_pir_init();

    //SERVO-MOTEUR (gun shot)
    gpio_init (PWM_PIN, GPIO_OUT);
    gun_state = 1;

    //CAYENNE (Lora communication to cayenne)
    cayenne();
    sender();

    printf("\n ********** END INITIALISATION ********** \n");
    printf("\n ********** ALARM STATE : OFF **********\n");

    //******************************* PRINCIPAL LOOP ********************************//

    while(1){

        //POWER OFF
        //printf("\n ALARM OFF\n");

        //POWER ON
        if(power_up()){

            printf("\n ********** POWER ON - ALARM **********\n");

            //Led
            gpio_write(LED_PIN, 1); //Switch on led

            //Delay
            xtimer_sleep(1);

            //ALARM ON
            while(1){

                //printf("\nALARM ON");

                    //********SCD30 Measurement*********//
                    if(sensor_scd30_measure()){
                        intrusion = 1;
                    }

                    //********SCD30 Measurement*********//
                    if(intrusion == 0){
                        if(sensor_pir_measurement()){
                            intrusion = 1;
                        }
                    }

                    //******** Send Data to cayenne *********//
                    if(intrusion == 0){
                        sender();
                    }

                    //******** Launch Shot+Alarm *********//
                    if(intrusion){


                        //Launch Shot
                        pwm_shot();

                        //Send datas (inform intrusion)
                        gun_state = 0;
                        alarm_state=1;
                        sender_ite = 3;
                        sender();

                        while(1){

                            //Start Alarm
                            alaarm(2);

                            //TEST -> ALARM POWER DOWN
                            button_state = gpio_read(BUTTON_PIN);
                            if(button_state == 16){
                                alarm_state = 0;
                                intrusion = 0;
                                button_state = 8;
                                gun_state = 1;
                                power_down_song();
                                gpio_write(LED_PIN, 0); //Switch off led
                                printf("\n ********** POWER DOWN - ALARM **********\n");
                                xtimer_usleep(15000);
                                break;
                            }
                        }
                    }
            }
        }
    }

    printf("\n MAIN FINISH \n");
    while(1)
        ;

    return 0;
}

/******************************* FUNCTIONS POWER UP *******************************/
//Power up the alarm (press button + song launch + launch led)
int power_up(void){

    button_state = gpio_read(BUTTON_PIN);
    //printf("\nbutton : %d", button_state);

    if(button_state == 16){
        xtimer_usleep(1000);
        button_state = gpio_read(BUTTON_PIN);
        if(button_state == 16){
            button_state = 8;
            printf("\nPOWER UP - ALARM");
            power_up_song();
            xtimer_sleep(1);
            gpio_write(LED_PIN, 1); //Switch on led
            return 1;
        }

        return 0;
    }
    return 0;
}




/******************************* FUNCTIONS BUZZER *******************************/

//Launch the alarm (buzzer and red led)
void alaarm(long int duration){

        int x = 0;

        //AU CLAIR DE LA LUNE
       //int freq_note[] = {392, 392, 392, 440, 493, 440, 392, 493, 440, 440, 392, 392, 392, 392, 440, 493, 440, 392, 493, 440, 440, 392, 440, 440, 440, 440, 330, 330, 440, 392, 349, 330, 294, 392, 392, 392, 440, 493, 440, 392, 493, 440, 440, 392};
       //int duree[] = {4, 4, 4, 4, 8, 8, 4, 4, 4, 4, 16, 4, 4, 4, 4, 8, 8, 4, 4, 4, 4, 16, 4, 4, 4, 4, 8, 8, 4, 4, 4, 4, 16, 4, 4, 4, 4, 8, 8, 4, 4, 4, 4, 16};

        //PIRATES DES CARAÎBES
        //uint16_t freq_note [] = {659, 784, 880, 880, 0, 880, 988, 1047, 1047, 0, 1047, 1175, 988, 988, 0, 880, 784, 880, 0, 659, 784, 880, 880, 0, 880, 988, 1047, 1047, 0, 1047, 1175, 988, 988, 0, 880, 784, 880, 0, 659, 784, 880, 880, 0, 880, 1047, 1175, 1175, 0, 1175, 1319, 1397, 1397, 0, 1319, 1175, 1319, 880, 0, 880, 988, 1047, 1047, 0, 1175, 1319, 880, 0, 880, 1047, 988, 988, 0, 1047, 880, 988, 0, 880, 880, 880, 988, 1047, 1047, 0, 1047, 1175, 988, 988, 0, 880, 784, 880, 0, 659, 784, 880, 880, 0, 880, 988, 1047, 1047, 0, 1047, 1175, 988, 988, 0, 880, 784, 880, 0, 659, 784, 880, 880, 0, 880, 1047, 1175, 1175, 0, 1175, 1319, 1397, 1397, 0, 1319, 1175,  1319, 880, 0, 880, 988, 1047, 1047, 0, 1175, 1319, 880, 0, 880, 1047, 988, 988, 0, 1047, 880, 988, 0, 1319, 0, 0, 1397, 0, 0, 1319, 1319, 0, 1568, 0, 1319, 1175, 0, 0, 1175, 0, 0, 1047, 0, 0, 988, 1047, 0, 988, 0, 880, 1319, 0, 0, 1397, 0, 0, 1319, 1319, 0, 1568, 0, 1319, 1175, 0, 0, 1175, 0, 0, 1047, 0, 0, 988, 1047, 0, 988, 0, 880};
        //uint16_t duree [] = {1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 3, 1, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 3, 1, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 2, 1, 1, 2, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 3, 3, 2, 1, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 3, 1, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 3, 1, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 2, 1, 1, 2, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 3, 3, 2, 1, 3, 2, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 3, 2, 1, 3, 2, 1, 3, 1, 1, 1, 1, 1, 4, 2, 1, 3, 2, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 3, 2, 1, 3, 2, 1, 3, 1, 1, 1, 1, 1, 4};
        //uint16_t limit_k = 200; //+ 220-230

        //ALARME AIGU
        int freq_note[] = {200,300};
        int duree[] = {5, 5};
        uint16_t limit_k = 2;

        //DURÉE DE L'ALARME
        for (int l = 0; l < duration; l++) {
            
            x = x^1;
            gpio_write(LED_PIN, x); //D5, 1

                for(int k=0; k < limit_k; k++){

                    for (int i=0; i < (duree[k]*20); i++) {
                        
                        // NOTE (55)
                        gpio_write(BUZZER_PIN, 1); //D5, 1
                        xtimer_usleep(100000/(2*freq_note[k]));
                        gpio_write(BUZZER_PIN,0);
                        xtimer_usleep(100000/(2*freq_note[k]));
                    }
                }
            }

}

void power_up_song(void){

        printf("\nPOWER UP - SONG");

        //ALARME AIGU
        int freq_note[] = {100,200,300,400};
        int duree[] = {100, 100, 100, 200};

        //DURÉE DE L'ALARME
        for (int l = 0; l < 1; l++) {

                for(int k=0; k < 4; k++){

                    for (int i=0; i < (duree[k]*400/150); i++) {
                        
                        // NOTE
                        gpio_write(BUZZER_PIN, 1); //D5, 1
                        xtimer_usleep(100000/(2*freq_note[k]));
                        gpio_write(BUZZER_PIN, 0);
                        xtimer_usleep(100000/(2*freq_note[k]));
                    }
                }
            }
}

void power_down_song(void){

        printf("\nPOWER DOWN - SONG");

        //ALARME AIGU
        int freq_note[] = {400,300,200,100};
        int duree[] = {100, 100, 100, 200};

        //DURÉE DE L'ALARME
        for (int l = 0; l < 1; l++) {

                for(int k=0; k < 4; k++){

                    //printf("test note : %d\n", k);

                    for (int i=0; i < (duree[k]*400/150); i++) {
                        
                        // NOTE
                        gpio_write(BUZZER_PIN, 1); //D5, 1
                        xtimer_usleep(100000/(2*freq_note[k]));
                        gpio_clear(BUZZER_PIN);
                        xtimer_usleep(100000/(2*freq_note[k]));
                    }
                }
            }
            

}

/******************************* FUNCTIONS SHOT *******************************/
void pwm_shot(void){

        printf("\n*******PWM SHOOOOT - be carefull********\n");
        for (int a = 0; a < 80; a++) {

                        gpio_write(PWM_PIN, 1); //D5, 1
                        xtimer_usleep(10000);
                        gpio_clear(PWM_PIN); 
                        xtimer_usleep(10000);
        }


}


/******************************* FUNCTIONS CAYENNE *******************************/

//Init Lora connexion with Cayenne
void cayenne(void){
    printf("\n***********INITIALISATION - Connexion to cayenne*************\n");
    /*----------------------------------------------------------- BEGIN CAYENNE -----------------------------------------------------------*/
    /* use a fast datarate so we don't use the physical layer too much */
    semtech_loramac_set_dr(&loramac, 5);

    /* set the LoRaWAN keys */
    semtech_loramac_set_deveui(&loramac, deveui);
    semtech_loramac_set_appeui(&loramac, appeui);
    semtech_loramac_set_appkey(&loramac, appkey);

    //start the OTAA join procedure 
    puts("Starting join procedure");
    if (semtech_loramac_join(&loramac, LORAMAC_JOIN_OTAA) != SEMTECH_LORAMAC_JOIN_SUCCEEDED) {
        puts("Join procedure failed");
        return;
    }
    puts("Join procedure succeeded");

    /* call the sender function */
    sender();

    return;
}

//Send informations to cayenne by Lora communication
static void sender(void){

    /* update variable */
    sender_ite++;

    if(sender_ite > 2){

            printf("\nSEND DATAS TO CAYENNE - Lora Communication\n");

            /* TODO: prepare cayenne lpp payload here */
            cayenne_lpp_add_temperature(&lpp, 0, (float)TEMP_value);
            cayenne_lpp_add_presence(&lpp, 1, (uint8_t)intrusion);
            cayenne_lpp_add_digital_input(&lpp, 2, (uint8_t)gun_state);
            cayenne_lpp_add_digital_input(&lpp, 3, (uint8_t)alarm_state);
            cayenne_lpp_add_relative_humidity(&lpp, 4, (float) HUMIDITY_value);
            cayenne_lpp_add_analog_input(&lpp, 5, (float) CO2_value/10);
            cayenne_lpp_add_digital_input(&lpp, 6, (uint8_t)fire_detection);
            cayenne_lpp_add_digital_input(&lpp, 7, (uint8_t)pollution_detection);
            cayenne_lpp_add_digital_input(&lpp, 8, (uint8_t)flood_detection);

            printf("Sending LPP data\n");

            /* send the LoRaWAN message */
            uint8_t ret = semtech_loramac_send(&loramac, lpp.buffer, lpp.cursor);
            if (ret != SEMTECH_LORAMAC_TX_DONE) {
                printf("Cannot send lpp message, ret code: %d\n", ret);
            }

            /* TODO: clear buffer once done here */
            cayenne_lpp_reset(&lpp);
        
        sender_ite = 0;

    }

        return;
}


/******************************* FUNCTIONS SENSOR SCD30 *******************************/

//Init the SCD30 sensor
void sensor_scd30_init(void){

     /*------ BEGIN SENSOR SCD30 ------*/

    printf("\n-------------------INIT SENSOR SCD30-------------------\n");
    //printf("\nSCD30 Test:\n");
    int i = 0;
    //int TEST_ITERATIONS=0;

    scd30_init(&scd30_dev, &params);
    uint16_t pressure_compensation = SCD30_DEF_PRESSURE;
    uint16_t value = 0;
    uint16_t interval = MEASUREMENT_INTERVAL_SECS;

    scd30_set_param(&scd30_dev, SCD30_INTERVAL, MEASUREMENT_INTERVAL_SECS);
    scd30_set_param(&scd30_dev, SCD30_START, pressure_compensation);

    scd30_get_param(&scd30_dev, SCD30_INTERVAL, &value);
    printf("[test][dev-%d] Interval: %u s\n", i, value);
    scd30_get_param(&scd30_dev, SCD30_T_OFFSET, &value);
    printf("[test][dev-%d] Temperature Offset: %u.%02u C\n", i, value / 100u, value % 100u);
    scd30_get_param(&scd30_dev, SCD30_A_OFFSET, &value);
    printf("[test][dev-%d] Altitude Compensation: %u m\n", i, value);
    scd30_get_param(&scd30_dev, SCD30_ASC, &value);
    printf("[test][dev-%d] ASC: %u\n", i, value);
    scd30_get_param(&scd30_dev, SCD30_FRC, &value);
    printf("[test][dev-%d] FRC: %u ppm\n", i, value);

    i = 0;
    scd30_start_periodic_measurement(&scd30_dev, &interval, &pressure_compensation);
    
    while (i < 5) {
        xtimer_sleep(MEASUREMENT_INTERVAL_SECS);
        scd30_read_periodic(&scd30_dev, &result);
        scd30_read_triggered(&scd30_dev, &result);
        printf("[scd30_test-%d] Continuous measurements co2: %.02fppm,"" temp: %.02f°C, hum: %.02f%%. \n", i, result.co2_concentration, result.temperature, result.relative_humidity);
        i++;
    }

        CO2_value = result.co2_concentration;
        TEMP_value = result.temperature;
        HUMIDITY_value = result.relative_humidity;

    
    scd30_stop_measurements(&scd30_dev);

    printf("\n ----- SCD30 STOP -----\n");

    /*------- END SENSOR SCD30 ------*/

    return;

}

//Get a SCD30 sensor measure
int sensor_scd30_measure(void){


        CO2_previous_value = CO2_value;
        TEMP_previous_value = TEMP_value;
        HUMIDITY_previous_value = HUMIDITY_value;

        uint16_t pressure_compensation = SCD30_DEF_PRESSURE;
        uint16_t interval = MEASUREMENT_INTERVAL_SECS;
        scd30_start_periodic_measurement(&scd30_dev, &interval, &pressure_compensation);        
        xtimer_sleep(MEASUREMENT_INTERVAL_SECS);
        scd30_read_periodic(&scd30_dev, &result);
        scd30_read_triggered(&scd30_dev, &result);
        printf("\nSCD30 PREVIOUS MEASUREMENT --> co2: %.02fppm,"" temp: %.02f°C, hum: %.02f%%. \n", CO2_previous_value, TEMP_previous_value, HUMIDITY_previous_value);
        printf("\nSCD30 ONE MEASUREMENT --> co2: %.02fppm,"" temp: %.02f°C, hum: %.02f%%. \n", result.co2_concentration, result.temperature, result.relative_humidity);
        
        scd30_stop_measurements(&scd30_dev);

        CO2_value = result.co2_concentration;
        TEMP_value = result.temperature;
        HUMIDITY_value = result.relative_humidity;

        //Intrusion detection with CO2 value
        if(CO2_value > (CO2_previous_value+5000)){
            printf("\n INTRUSION - CO2 detection \n");
            scd30_stop_measurements(&scd30_dev);
            return 1;
        }

        //Intrusion detection with TEMP value
        if(TEMP_value > (TEMP_previous_value+3)){
            printf("\n INTRUSION - TEMPERATURE detection \n");
            scd30_stop_measurements(&scd30_dev);
            return 1;
       }

        //Intrusion detection with HUMIDITY value
        if(HUMIDITY_value > (HUMIDITY_previous_value+3)){
            printf("\n INTRUSION - HUMIDITY detection \n");
            scd30_stop_measurements(&scd30_dev);
            return 1;
        }

        //Fire detection
        if(TEMP_value > 30){
            printf("\n FIRE detection \n");
            fire_detection = 1;
            scd30_stop_measurements(&scd30_dev);
            return 1;
        }

        //Pollution detection
        if(CO2_value > 7000){
            printf("\n POLLUTION detection \n");
            pollution_detection = 1;
            scd30_stop_measurements(&scd30_dev);
            return 1;
        }

        //Flood detection
        if(HUMIDITY_value > 80){
            printf("\n HUMIDITY detection \n");
            flood_detection = 1;
            scd30_stop_measurements(&scd30_dev);
            return 1;
        }
        //scd30_stop_measurements(&scd30_dev);

        return 0;
        

}




/******************************* FUNCTIONS SENSOR PIR *******************************/

//Init the sensor PIR
void sensor_pir_init(void){

    /*--------- BEGIN SENSOR PIR ---------*/
    puts("PIR motion sensor test application\n");
    printf("\nInitializing PIR sensor at GPIO_%ld... ", (long)PIR_PARAM_GPIO);
    if (pir_init(&dev, &pir_params[0]) == 0) {
        puts("[OK]\n");
        xtimer_sleep(1);
    }
    else {
        puts("[Failed]");
        xtimer_sleep(1);
        return;
    }

    return;

}

//Get the state of PIR detection (0 : nothing - 1 : detect - 2 : error)
int sensor_pir_measurement(void){

    printf("\nPIR MEASUREMENT");

    int pir_dection = 0;

    for(int o=0; o <10; o++){

        xtimer_sleep(1);
        printf("\npir value : %d", pir_get_status(&dev));
        if(pir_get_status(&dev) == 150){
            printf("\nDetect 1 Movement");
            pir_dection++;
            return 1;
        }
    }

    if(pir_dection < pir_detection_threshold)
        return 0;
    else 
        return 1;
}
